#!/usr/bin/env python3

"""
Copyright 2021 Tiago Epifanio

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import getopt
import sys
import re
import math

ABOUT_TEXT = '\
HDF Create v0.1\n\
Copyright 2021 Tiago Epifanio\n\
\n\
Licensed under the Apache License, Version 2.0 (the "License");\n\
you may not use this file except in compliance with the License.\n\
You may obtain a copy of the License at\n\
\n\
    http://www.apache.org/licenses/LICENSE-2.0\n\
\n\
Unless required by applicable law or agreed to in writing, software\n\
distributed under the License is distributed on an "AS IS" BASIS,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n\
See the License for the specific language governing permissions and\n\
limitations under the License.\n\
'

HELP = '\
HDF Create v0.1\n\
Copyright 2021 Tiago Epifanio\n\
This is free software, and you are welcome to redistribute it\n\
under certain conditions; type \'python3 hdfcreate --about\' for details.\n\n\
Usage: python3 hdfcreate.py [OPTIONS]\n\n\
Options:\n\
    -t, --type TYPE\n\
         TYPE: raw (Single partition)\n\
               rdb (Partitionable Hard Drive Image)\n\
    -s, --size SYZE\n\
    -f, --filename\n\
    -h, --help\n\n\
Example 1 - create a single partition file with 32GiB:\n\
    python3 hdfcreate.py --type raw --size 32G --filename myhd.hdf\n\n\
Example 1 - create a partitionable hard drive with 512MiB:\n\
    python3 hdfcreate.py --type rdb --size 512M --filename myhd.hdf\n\
'

DISK_TYPE_RAW = "raw"
DISK_TYPE_RDB = "rdb"
DISK_TYPES = [DISK_TYPE_RAW,DISK_TYPE_RDB]

def create_disk(size, filename = "out.hdf", disktype = "raw"):
    if disktype not in DISK_TYPES:
        print("Invalid type. Valid types are: raw, rdb.")

    size_regex = re.compile('(\d+)([bkmgBKMG]{0,1}$)')

    m = size_regex.match(size)

    if m:
        size = int(m.group(1))
        unit = m.group(2)
    else:
        print("Invalid size.")
        sys.exit(1)

    if (unit in ("k","K")):
        size *= 1024
    elif (unit in ("m","M")):
        size *= 1024 * 1024
    elif (unit in ("g","G")):
        size *= 1024 * 1024 * 1024

    try:
        f = open(filename, "wb")
    except Exception:
        print("Error opening file.")
        sys.exit(1)

    if (disktype == DISK_TYPE_RDB):
        size = size - 4
        f.write(b"rdsk")

    blocksize = 1024 * 1024 * 10; #10MiB
    written = 0

    for i in range(0, size // (blocksize)):
        f.write(b"\0" * blocksize)
        written += blocksize
        print("\rProgress: " + str(math.floor(written / size * 100)) + "%",end='',flush=True)

    f.write(b"\0" * (size % blocksize))

    print("\rProgress: 100%")
def main(argv):
    if len(argv) == 0:
        print(HELP)
        sys.exit(1)

    disktype = None
    size = None
    filename = None

    try:
        opts, args = getopt.getopt(argv[0:],"t:s:f:h",["type=","size=","filename=","about","help"])
    except getopt.GetoptError:
        print(HELP)
        sys.exit(1)

    for opt, arg in opts:
        if opt in ("-t", "--type"):
            disktype = arg
        elif opt in ("-s", "--size"):
            size = arg
        elif opt in ("-f", "--filename"):
            filename = arg
        elif opt in ("-h", "--help"):
            print(HELP)
            sys.exit(1)
        elif opt in ("--about"):
            print(ABOUT_TEXT)
            sys.exit(1)

    if size is None:
        print("Size not specified. Use -s parameter to specify size.")
        sys.exit(1)

    create_disk(size, filename, disktype)

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except Exception as ex:
        print('Error: ' + str(ex))

# HDF Create
Create Amiga Hard Disk Images (HDF)

# How to use
```
Usage: python3 hdfcreate.py [OPTIONS]
Options:
    -t, --type TYPE
         TYPE: raw (Single partition)
               rdb (Partitionable Hard Drive Image)
    -s, --size SYZE
    -f, --filename
    -h, --help
Example 1 - create a single partition file with 32GiB:
    python3 hdfcreate.py --type raw --size 32G --filename myhd.hdf
Example 1 - create a partitionable hard drive with 512MiB:
    python3 hdfcreate.py --type rdb --size 512M --filename myhd.hdf
```

# Copyright
Copyright (C) 2021 Tiago Epifânio

# License
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
